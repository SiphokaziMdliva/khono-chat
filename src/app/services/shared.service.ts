import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable()
export class SharedService {
    selectedId: string;
    constructor(
        public afAuth: AngularFireAuth,
        public db: AngularFirestore,
        public router: Router
    ) {}

    setSelectedId(id) {
        this.selectedId = id;
    }
}
